# raspi_sd_fw

#### 介绍
用于存放树莓派SD卡各种固件的信息。

目录结构

```
raspi2
raspi3-32
raspi3-64
raspi4-32
raspi4-64
```

以上目录用于存放各种树莓派的启动引导rt-thread的固件信息